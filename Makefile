CURRENT_DIR=$(shell pwd)

migrate-up:
	@migrate -database postgres://postgres:password@localhost:5432/catalog_service\?sslmode\=disable -path migrations up

migrate-down:
	@migrate -database postgres://postgres:password@localhost:5432/catalog_service\?sslmode\=disable -path migrations down

pull-proto-module:
	git submodule update --init --recursive

update-proto-module:
	git submodule update --remote --merge

proto-gen:
	./scripts/gen-proto.sh  ${CURRENT_DIR}

run:
	@go run cmd/main.go
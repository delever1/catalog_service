package main

import (
	"context"
	"delever/catalog_service/config"
	"delever/catalog_service/grpc"
	"delever/catalog_service/grpc/client"
	"delever/catalog_service/pkg/logger"
	"delever/catalog_service/storage/postgres"
	"net"
)

func main() {
	// Load Config
	cfg := config.Load()

	// Setup Logger
	loggerLevel := logger.LevelDebug
	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
	case config.TestMode:
		loggerLevel = logger.LevelDebug
	default:
		loggerLevel = logger.LevelInfo
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	// Connect to Database
	pgconn, err := postgres.NewPostgres(context.Background(), cfg)
	if err != nil {
		log.Panic("Postgres no connection: ", logger.Error(err))
	}
	defer pgconn.CloseDB()

	// Connect To Server
	srvc, err := client.NewGrpcClients(cfg)
	if err != nil {
		log.Panic("Service no connection: ", logger.Error(err))
	}

	grpcServer := grpc.SetUpServer(cfg, log, pgconn, srvc)

	// Listen Port
	lis, err := net.Listen("tcp", cfg.CatalogGRPCPort)
	if err != nil {
		log.Panic("net.listen", logger.Error(err))
	}

	log.Info("GRPC: Server being started...", logger.String("port", cfg.CatalogGRPCPort))

	err = grpcServer.Serve(lis)
	if err != nil {
		log.Panic("grpcServer.Server", logger.Error(err))
	}
}

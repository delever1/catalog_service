package postgres

import (
	"context"
	"delever/catalog_service/genproto/catalog_service"
	"fmt"

	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
)

type ProductRepo struct {
	db *pgxpool.Pool
}

func NewProductRepo(db *pgxpool.Pool) *ProductRepo {
	return &ProductRepo{
		db: db,
	}
}

func (r *ProductRepo) Create(ctx context.Context, req *catalog_service.ProductCreateReq) (*catalog_service.ProductCreateResp, error) {
	var id int
	query := `
	INSERT INTO products(
		title,
		description,
		photo,
		type,
		price,
		category_id
	)
	VALUES($1,$2,$3,$4,$5,$6)
	RETURNING id;`

	if err := r.db.QueryRow(ctx, query,
		req.Title,
		req.Description,
		req.Photo,
		req.Typ,
		req.Price,
		req.CategoryId,
	).Scan(&id); err != nil {
		return nil, err
	}

	return &catalog_service.ProductCreateResp{Msg: fmt.Sprintf("%d", id)}, nil
}

func (r *ProductRepo) GetList(ctx context.Context, req *catalog_service.ProductGetListReq) (*catalog_service.ProductGetListResp, error) {
	var (
		filter  = " WHERE deleted_at IS NULL "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		id,
		title,
		description,
		photo,
		order_number,
		active::TEXT,
		type,
		price,
		category_id,
		created_at::TEXT
	FROM products `

	if req.Title != "" {
		filter += " AND title ILIKE " + "'%" + req.Title + "%' "
	}
	if req.Typ != "" {
		filter += " AND type = '" + req.Typ + "' "
	}
	if req.CategoryId != "" {
		filter += " AND category_id = '" + req.CategoryId + "' "
	}
	if req.Active != "" {
		filter += " AND active = '" + req.Active + "' "
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM products` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp := &catalog_service.ProductGetListResp{}
	for rows.Next() {
		var product = catalog_service.Product{}
		err := rows.Scan(
			&product.Id,
			&product.Title,
			&product.Description,
			&product.Photo,
			&product.OrderNumber,
			&product.Active,
			&product.Typ,
			&product.Price,
			&product.CategoryId,
			&product.CreatedAt,
		)

		if err != nil {
			return nil, err
		}
		resp.Products = append(resp.Products, &product)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *ProductRepo) GetById(ctx context.Context, req *catalog_service.ProductIdReq) (*catalog_service.Product, error) {
	query := `
	SELECT 
		id,
		title,
		description,
		photo,
		order_number,
		active,
		type,
		price,
		category_id,
		created_at::TEXT 
	FROM products 
	WHERE id=$1 AND deleted_at IS NULL;`

	var product = catalog_service.Product{}
	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&product.Id,
		&product.Title,
		&product.Description,
		&product.Photo,
		&product.OrderNumber,
		&product.Active,
		&product.Typ,
		&product.Price,
		&product.CategoryId,
		&product.CreatedAt,
	); err != nil {
		return nil, err
	}

	return &product, nil
}

func (r *ProductRepo) Update(ctx context.Context, req *catalog_service.ProductUpdateReq) (*catalog_service.ProductUpdateResp, error) {
	query := `
	UPDATE products 
	SET 
		title=$2,
		description=$3,
		photo=$4,
		active=$5,
		type=$6,
		price=$7,
		category_id=$8,
		updated_at=NOW() 
	WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
		req.Title,
		req.Description,
		req.Photo,
		req.Active,
		req.Typ,
		req.Price,
		req.CategoryId,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}
	return &catalog_service.ProductUpdateResp{Msg: "OK"}, nil
}

func (r *ProductRepo) Delete(ctx context.Context, req *catalog_service.ProductIdReq) (*catalog_service.ProductDeleteResp, error) {
	query := `
    UPDATE products 
    SET 
        deleted_at=NOW() 
    WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &catalog_service.ProductDeleteResp{Msg: "OK"}, nil

}

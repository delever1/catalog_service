package service

import (
	"context"
	"delever/catalog_service/config"
	"delever/catalog_service/genproto/catalog_service"
	"delever/catalog_service/grpc/client"
	"delever/catalog_service/pkg/logger"
	"delever/catalog_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*catalog_service.UnimplementedProductServiceServer
}

func NewProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ProductService {
	return &ProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ProductService) Create(ctx context.Context, req *catalog_service.ProductCreateReq) (*catalog_service.ProductCreateResp, error) {
	u.log.Info("====== Product Create ======", logger.Any("req", req))

	resp, err := u.strg.Product().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ProductService) GetList(ctx context.Context, req *catalog_service.ProductGetListReq) (*catalog_service.ProductGetListResp, error) {
	u.log.Info("====== Product GetList ======", logger.Any("req", req))

	resp, err := u.strg.Product().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while get list product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ProductService) GetById(ctx context.Context, req *catalog_service.ProductIdReq) (*catalog_service.Product, error) {
	u.log.Info("====== Product GetById ======", logger.Any("req", req))

	resp, err := u.strg.Product().GetById(ctx, req)
	if err != nil {
		u.log.Error("error while get product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ProductService) Update(ctx context.Context, req *catalog_service.ProductUpdateReq) (*catalog_service.ProductUpdateResp, error) {
	u.log.Info("====== Product Update ======", logger.Any("req", req))

	resp, err := u.strg.Product().Update(ctx, req)
	if err != nil {
		u.log.Error("error while update product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ProductService) Delete(ctx context.Context, req *catalog_service.ProductIdReq) (*catalog_service.ProductDeleteResp, error) {
	u.log.Info("====== Product Delete ======", logger.Any("req", req))

	resp, err := u.strg.Product().Delete(ctx, req)
	if err != nil {
		u.log.Error("error while delete product", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

package grpc

import (
	"delever/catalog_service/config"
	"delever/catalog_service/genproto/catalog_service"
	"delever/catalog_service/grpc/client"
	"delever/catalog_service/grpc/service"
	"delever/catalog_service/pkg/logger"
	"delever/catalog_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	catalog_service.RegisterProductServiceServer(grpcServer, service.NewProductService(cfg, log, strg, srvc))
	catalog_service.RegisterCategoryServiceServer(grpcServer, service.NewCategoryService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
